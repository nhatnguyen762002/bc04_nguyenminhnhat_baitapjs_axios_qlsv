function resetForm() {
  document.querySelector("#txtMaSV").value = "";
  document.querySelector("#txtTenSV").value = "";
  document.querySelector("#txtEmail").value = "";
  document.querySelector("#txtPass").value = "";
  document.querySelector("#txtDiemToan").value = "";
  document.querySelector("#txtDiemLy").value = "";
  document.querySelector("#txtDiemHoa").value = "";
}

function renderDSSV(dssv) {
  var contentHTML = "";

  dssv.forEach(function (sv) {
    var contentStr = `<tr>
    <td>${sv.id}</td>
    <td>${sv.name}</td>
    <td>${sv.email}</td>
    <td>${((sv.math * 1 + sv.physics * 1 + sv.chemistry * 1) / 3).toFixed(
      1
    )}</td>
    <td>
    <button class="btn btn-danger" onclick="xoaSV('${sv.id}')">Xóa</button>
    <button class="btn btn-warning" onclick="suaSV('${sv.id}')">Sửa</button>
    </td>
    </tr>`;

    contentHTML += contentStr;
  });

  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function batLoading() {
  document.getElementById("loading").style.display = "flex";
}

function tatLoading() {
  document.getElementById("loading").style.display = "none";
}

function layThongTinTuForm() {
  const maSvValue = document.querySelector("#txtMaSV").value;
  const tenSvValue = document.querySelector("#txtTenSV").value;
  const emailValue = document.querySelector("#txtEmail").value;
  const matKhauValue = document.querySelector("#txtPass").value;
  const diemToanValue = document.querySelector("#txtDiemToan").value;
  const diemLyValue = document.querySelector("#txtDiemLy").value;
  const diemHoaValue = document.querySelector("#txtDiemHoa").value;

  return new SinhVien(
    tenSvValue,
    emailValue,
    matKhauValue,
    diemToanValue,
    diemLyValue,
    diemHoaValue,
    maSvValue
  );
}

function showThongTinLenForm(sv) {
  document.querySelector("#txtMaSV").value = sv.id;
  document.querySelector("#txtTenSV").value = sv.name;
  document.querySelector("#txtEmail").value = sv.email;
  document.querySelector("#txtPass").value = sv.password;
  document.querySelector("#txtDiemToan").value = sv.math;
  document.querySelector("#txtDiemLy").value = sv.physics;
  document.querySelector("#txtDiemHoa").value = sv.chemistry;
}

function layID(dssv) {
  return dssv.map(function (sv) {
    return sv.id;
  });
}
