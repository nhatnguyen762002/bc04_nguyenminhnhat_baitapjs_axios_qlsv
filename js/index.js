const BASE_URL = "https://62db6cd9e56f6d82a77287f5.mockapi.io";

function getDSSV() {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      renderDSSV(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
}

getDSSV();

function xoaSV(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function () {
      tatLoading();
      resetForm();
      getDSSV();
    })
    .catch(function (err) {
      tatLoading();
      resetForm();
      console.log("err: ", err);
    });
}

function themSV() {
  var newSV = layThongTinTuForm();

  batLoading();

  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: newSV,
  })
    .then(function (res) {
      tatLoading();
      resetForm();
      getDSSV();
      console.log("res: ", res);
    })
    .catch(function (err) {
      tatLoading();
      resetForm();
      console.log("err: ", err);
    });
}

function suaSV(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      showThongTinLenForm(res.data);
      document.getElementById("btn-update").disabled = false;
      document.getElementById("btn-them").disabled = true;
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
}

function updateSV() {
  var newSV = layThongTinTuForm();

  batLoading();

  axios({
    url: `${BASE_URL}/sv/${newSV.id}`,
    method: "PUT",
    data: newSV,
  })
    .then(function () {
      tatLoading();
      resetForm();
      document.getElementById("btn-them").disabled = false;
      document.getElementById("btn-update").disabled = true;
      getDSSV();
    })
    .catch(function (err) {
      tatLoading();
      resetForm();
      console.log("err: ", err);
    });
}

function resetSV() {
  resetForm();
  document.getElementById("btn-update").disabled = true;
  document.getElementById("btn-them").disabled = false;
}
